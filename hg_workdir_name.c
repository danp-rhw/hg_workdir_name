#include <stdio.h>
#include <stdlib.h>

#include <libgen.h>
#include <unistd.h>


#define BUFFSIZE 1024

// Number of directories to traverse before giving up
#define MAX_TRIES 25


int main(int argc, char **argv) {
    char startdir[BUFFSIZE];  // The user's CWD
    char filename[BUFFSIZE];  // The filename to try
    char hgname[BUFFSIZE];    // The user's hg branch or bookmark
    FILE *infile = NULL;      // A handle to filename, if it exists
    char *curdir = startdir;
    int try_count = 0;

    // start off at current directory
    getcwd(startdir, sizeof(startdir));
    for (; try_count < MAX_TRIES && infile == NULL; try_count++) {
        snprintf(filename, BUFFSIZE, "%s/.hg/bookmarks.current", curdir);
        infile = fopen(filename, "r");
        if (infile == NULL) {
            snprintf(filename, BUFFSIZE, "%s/.hg/branch", curdir);
            infile = fopen(filename, "r");
        }
        curdir = dirname(curdir);
    }

    if (try_count >= MAX_TRIES) {
        return 1;  // Not in an hg repo
    }

    // This should never happen
    if (infile == NULL) {
        fprintf(stderr, "Unexpected program error.\n");
        return 2;
    }

    fgets(hgname, BUFFSIZE, infile);
    printf("%s", hgname);
    return 0;
}
