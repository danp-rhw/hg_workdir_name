hg_workdir_name
---------------

This is a tiny utility to print either the current bookmark or current branch
of a Mercurial repository. This program runs much quicker than Mercurial does,
e.g. a quick `perf stat -r 10 -d` on my machine measure `hg root` as taking an
average of 50ms per execution, where this program takes 0.3ms on average.

Why should you care?

Because you can put your branch/bookmark in your terminal without having it be
laggy. Try using `hg book` AND `hg branch` in the same command to calculate it
the simple way. If you try and include that in your prompt you will see just
how laggy it gets! SPOILERS: it gets kinda laggy!!
